import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Color } from "./Utils/Color";
import { DP } from "./Utils/Dimen";

export default Checkbox = ({
  toggleChecked,
  isChecked = false,
  size = 20,
  style,
  activeBackgroundColor = Color.LINK,
  inActiveBackgroundColor = Color.WHITE,
  borderColor = Color.LINK,
  ...props
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={[
        Styles.checkBox,
        {
          height: size,
          width: size,
          backgroundColor: isChecked
            ? activeBackgroundColor
            : inActiveBackgroundColor,
          borderColor
        },
        style
      ]}
      onPress={toggleChecked}
      {...props}
    >
      {isChecked ? (
        <MaterialIcons
          name="check"
          style={{
            color: Color.WHITE,
            fontSize: size - DP._4
          }}
        />
      ) : null}
    </TouchableOpacity>
  );
};

const Styles = StyleSheet.create({
  checkBox: {
    borderWidth: DP._2,
    borderRadius: DP._2,
    borderStyle: "solid"
  }
});
