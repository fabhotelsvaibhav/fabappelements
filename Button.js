import React from "react";
import { Text, TouchableOpacity, StyleSheet, View } from "react-native";
import { Color } from "./Utils/Color";
import { DP } from "./Utils/Dimen";
import ArrowIcon from "react-native-vector-icons/Ionicons";

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.THEME_YELLOW,
    paddingVertical: DP._11,
    paddingHorizontal: DP._16,
    borderRadius: DP._30
  },
  disabledBtn: {
    backgroundColor: Color.GREY_E8
  },
  text: {
    color: Color.BLUE_1E,
    fontSize: DP._16,
    lineHeight: DP._19,
    textAlign: "center",
    fontWeight: "500"
  },
  arrowIcon: {
    position: "absolute",
    top: 0,
    right: DP._13,
    bottom: 0,
    justifyContent: "center"
  },
  position_relative: {
    position:'relative'
  }
});

export const Button = ({
  onPress = () => {},
  children = "",
  buttonStyle,
  textStyle,
  shouldShowIcon = false,
  iconSize = DP._20,
  iconStyle,
  evenSpacing = false,
  ...props
}) => {
  const containerStyles = [styles.container];
  const textStyles = [styles.text]
  if (props.disabled) {
    containerStyles.push(styles.disabledBtn);
  }
  if (evenSpacing) {
    containerStyles.push({ flexDirection: 'row', alignItems: 'center' })
    textStyles.push({paddingRight: DP._20})
  }
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[containerStyles, buttonStyle]}
      {...props}
    >
      <Text style={[textStyles, textStyle]}>{children}</Text>
      {shouldShowIcon && (
        <View style={[evenSpacing ? styles.position_relative : styles.arrowIcon, iconStyle]}>
          <ArrowIcon name={"ios-arrow-round-forward"} size={iconSize} />
        </View>
      )}
    </TouchableOpacity>
  );
};