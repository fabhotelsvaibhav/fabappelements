import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Switch from "./Switch";
import { DP } from "./Utils/Dimen";

export const SwitchWithTitle = ({
  children = "",
  isChecked = false,
  toggleSwitch,
  switchStyle,
  ...props
}) => {
  return (
    <View style={{ flexDirection: "row", alignItems:'center', justifyContent: "space-between" }}>
      <Text style={Styles.text}>{children}</Text>
      <Switch
        toggleSwitch={toggleSwitch}
        isChecked={isChecked}
        style={switchStyle}
        {...props}
      />
    </View>
  );
};

const Styles = StyleSheet.create({
  text: {
    lineHeight: DP._16,
    fontSize: DP._14
  }
});
