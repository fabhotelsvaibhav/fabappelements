import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { Color } from "./Utils/Color";
import { DP } from "./Utils/Dimen";
import Switch from "./Switch";
import Checkbox from "./Checkbox";

export const TitleNSubtitle = ({
  toggleChecked,
  isChecked = false,
  size,
  switchStyle,
  checkboxStyle,
  leftImageSource,
  leftImageStyle,
  title,
  titleStyle,
  subTitleStyle,
  subTitle,
  rightButtonType= undefined,
  rightButtonStyle,
  ...props
}) => {
  return (
    <View style={Styles.container}>
      {leftImageSource ? <Image
        source={leftImageSource}
        style={[Styles.leftImage, leftImageStyle]}
      /> : null}
      <View style={Styles.textContainer}>
        <Text style={[Styles.heading, titleStyle]}>{title}</Text>
        <Text style={[Styles.subHeading, subTitleStyle]}>{subTitle}</Text>
      </View>
      {rightButtonType !== undefined ? (
        <View style={Styles.rightButton}>
          {renderButtonAtRight(rightButtonType, checkboxStyle, switchStyle, size, isChecked, toggleChecked, props)}
        </View>
      ) : null}
    </View>
  );
};

function renderButtonAtRight(buttonType, checkboxStyle, switchStyle, size, isChecked, toggleChecked, props) {
  switch (buttonType) {
    case 'Switch':
      return <Switch style={switchStyle} toggleSwitch={toggleChecked} isChecked={isChecked} {...props}/>;
    case 'Checkbox':
      return <Checkbox style={checkboxStyle} toggleChecked={toggleChecked} size={size} isChecked={isChecked} {...props}/>;
    default: return null;
  }
}

const Styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center"
  },
  leftImage: {
    height: DP._36,
    width: DP._36,
    backgroundColor: "rgb(255,70,0)",
    marginRight: DP._16
  },
  textContainer: {
    flex: 1,
    paddingRight: DP._20
  },
  heading: { fontSize: DP._14, lineHeight: DP._16, color: Color.BLUE_1E },
  subHeading: {
    fontSize: DP._12,
    lineHeight: DP._14,
    color: Color.BLUE_1E_50_OPACITY
  },
  rightButton: {}
});
