import React from "react";
import { Switch } from "react-native";

export default (SwitchButton = ({
  toggleSwitch = () => {},
  isChecked = false,
  style,
  ...props
}) => {
  return (
    <Switch
      value={isChecked}
      onValueChange={toggleSwitch}
      style={{
        transform: [{ scaleX: .8 }, { scaleY: .8 }],
        style
      }}
      hitSlop={{top: 10, bottom: 10, left: 20, right: 20}}
      {...props}
    />
  );
});
