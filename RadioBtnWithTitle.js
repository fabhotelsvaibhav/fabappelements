import React from "react";
import { View, Text, StyleSheet } from "react-native";
import RadioButton from "./RadioButton";
import { DP } from "./Utils/Dimen";

export const RadioButtonWithTitle = ({
  children = "",
  radioButtonstyle,
  isChecked = false,
  radioButtonSize = 18,
  toggleChecked,
  borderColor,
  ...props
}) => {
  return (
    <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
      <Text style={Styles.text}>{children}</Text>
      <RadioButton
        size={radioButtonSize}
        isChecked={isChecked}
        style={radioButtonstyle}
        toggleChecked={toggleChecked}
        borderColor={borderColor}
        {...props}
      />
    </View>
  );
};

const Styles = StyleSheet.create({
  text: { lineHeight: DP._16, fontSize: DP._14 }
});
