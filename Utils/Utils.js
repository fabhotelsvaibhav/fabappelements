'Use Strict';

import {Platform, Dimensions} from 'react-native';

export function calculateDimen(size) {
    if (size === 1) {
        return size
    }
    if (isPlatformAndroid()) {
        size = 0.88 * size;
    }
    // if ((PixelRatio.get() > 2 && Platform.OS !== 'ios') || (Dimensions.get("window").width > 320 &&  Platform.OS === 'ios')){
    if (Dimensions.get("window").width > 320) {
        return (
            size
        )
    } else {
        return (
            size * 0.88
        )
    }
}

function isPlatformAndroid() {
    return Platform.OS === 'android';
}

export const family = {
    'regular': 'Roboto',
    'light': 'Roboto-Light',
    'bold': 'Roboto-Bold',
    'medium': Platform.OS === 'ios' ? 'Roboto-Medium' : 'Roboto_medium'
};

export const Utils = {
    isPlatformAndroid
}


