'Use Strict'

export const Color = {
    LIGHT_BLUE: '#E9F1FB',
    WHITE: '#ffffff',
    BLACK: '#000000',
    THEME_YELLOW: '#fddc2c',
    LIGHT_YELLOW: '#fffefa',
    THEME_BLUE: '#202C60',
    FACEBOOK_BLUE: "#3b5997",
    GOOGLE_RED: "#ed412d",
    LINK: '#379aff',
    LINK_LIGHT: '#48abff',
    FAB_BLUE_COLOR_DARK: '#181e3f',
    FAB_TAB_BLUE_COLOR: "#111831",
    ALICE_BLUE: '#ebf4ff',
    GREY_55: '#555555',
    GREY_E8: '#e8e8e8',
    DARK_SLATE_BLUE: '#1f2b64',
    ORANGE: '#EC9570',
    GREY_DF: '#DFDFDF',
    GREY_BD: '#bdbdbd',
    GREY_BE: '#bebebe',
    GREY_79: '#797979',
    GREY_E2: '#E2E2E2',
    THEME_GREY: '#c8c7c3',
    GREY_82: '#828282',
    GREY_83: '#838383',
    GREY_36: '#363636',
    GREY_4A: '#4a4a4a',
    GREY_57: '#575757',
    THEME_CYAN: '#a3d6d2',
    BACKGROUND: '#f0f3f7',
    GREY_9C: '#9c9c9c',
    THEME_GREEN: '#00b96e',
    GREEN_WITH_OPACITY: 'rgba(0, 255, 152, 0.5)',
    RED: '#FF0000',
    PALE_YELLOW: '#FFF6C8',
    GREY_9B: '#9b9b9b',
    BREAKFAST: '#304e5e',
    LIGHT_RED: '#cb4a4a',
    LIGHT_GREY: '#b2b2b2',
    GREY_B4: '#b4b4b4',
    GREY_B8: '#b8b8b8',
    PINKISH_GREY: '#c5c5c5',
    ZED_BLACK: '#0000004d',
    BLACK_DARK: '#000000cc',
    DARK_RED: '#cb4a4a',
    PALE_GREY: '#eff1f8',
    LIGHT_GREEN: '#F2FBF7',
    GREY_C8: '#c8c8c8',
    GREY_D8: '#d8d8d8',
    GREY_9F: '#9f9f9f',
    GREY_1F: '#1f1f1f',
    GREY_97: '#979797',
    GREY_97_SHADOW: 'rgba(151, 151, 151, 0.5)',
    GREY_F3: '#f3f4f6',
    White_FC: '#fcfcfc',
    WHITE_DF: '#DFDFDF',
    GREY_3C: '#3c3c3c',
    GREY_80: '#3c3c3c80',
    YELLOW_F2: '#f2ce0c',
    YELLOW_5A: '#f2ae5a',
    GREY_DASHED_BORDER: 'rgba(200, 199, 195, 0.6)',
    GREY_9A: '#9A9A9A',
    GREY_f7: '#f7f7f7',
    BLACK_50: 'rgba(0,0,0,0.5)',
    GREEN_6A: '#6ac259',
    BLUE_F2: '#f2fbf7',
    PURPLE_F0: '#f0aa8d',
    GREY_6F: '#6f6f6f',
    RED_FF: '#CB4A4A',
    BLACK_77: '#777777',
    RED_DD: '#DDACAC',
    GREY_88: '#888888',
    GREY_EA: '#EAEAEA',
    BLACK_03: 'rgba(0,0,0,0.3)',
    GREY_EE: '#eeeeee',
    GREY_D3: '#d3d3d3',
    GREY_D5: '#d5d5d5',
    GREY_D6: '#d6d6d6',
    GREY_4E: '#4e4e4e',
    GREY_89: '#898989',
    BLACK_80: 'rgba(0,0,0,0.8)',
    WHITE_07: 'rgba(255, 255, 255, 0.7)',
    WHITE_02: 'rgba(255, 255, 255, 0.2)',
    WHITE_05: 'rgba(255, 255, 255, 0.5)',
    WHITE_09: 'rgba(255, 255, 255, 0.9)',
    GREY_99: '#999999',
    BLUE_GREY: '#BFCBD6',
    GREY_9D: '#9D9D9D',
    GREY_EC: '#ECECEC',
    PALE_DARK_BLUE: '#4B506E',
    cloudyBlue: '#abb3d9',
    BLUE_E1: '#E1ECF8',
    badgeColor: {
        blue: '#4A90E2',
        silver: '#AAAAAA',
        gold: '#F0D34F',
        platinum: '#6B1D69'
    },
    TORCH_RED: '#f52141',
    VENETIAN_RED: '#D0021B',
    TUTU_RED: '#f8e3e6',
    BEIGE: '#ffe9ce',
    FOOTER_BG: '#d9e1ec',
    BACKGROUND_05: 'rgba(240, 243, 247, 0.5)',
    BLUE_AF: '#afdae6',
    BLUE_74: '#74c1d6',
    THEME_YELLOW_DISABLED: 'rgba(253, 220, 44, 0.4)',
    THEME_YELLOW_DISABLED_OPACITY_COLOR: '#F8E99E',
    BLACK_TRANSPARENT: 'rgba(0, 0,0, 0.1)',
    ORANGE_BRAND: '#fc4c02',
    CYAN_ESCAPE: '#24d3c5',
    FERN_GREEN: '#5CB85C',
    GREY_E4:'#e4e4e4',
    GREY_LIGHT: '#797ca0',
    BLUE_03: '#0375be',
    DARK_BLUE_GREY : '#1e254a',
    GREEN_21: '#21bea2',
    GREY_9C9A : '#9c9a9a',
    GREY_EF: '#efefef',
    WHITE_F0: '#f0f0f0',
    GREY_70: '#707070',
    BLACK_31: '#313131',
    GREY_C9: '#c9c6c6',
    GREY_ED: '#edf0f4',
    ORANGE_F5: '#f5a623',
    GREY_58: '#585858',
    GREY_B9: '#b9b9b9',
    VICTORIA_BLUE_70: '#484B7FB3',
    BLUE_1E: '#1e244a',
    BLUE_1E_50_OPACITY: 'rgba(30, 36, 74, 0.5)',
    BLUE_9B: '#9bccff'
}
