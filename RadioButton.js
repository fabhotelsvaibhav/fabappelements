import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Color } from "./Utils/Color";
import { DP } from "./Utils/Dimen";

export default RadioButton = ({
  toggleChecked,
  isChecked = false,
  size,
  style,
  borderColor = Color.LINK,
  ...props
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={[Styles.radioButton, 
        {
          height: size,
          width: size,
          justifyContent: 'center',
          alignItems:'center',
          borderRadius: size/2,
          backgroundColor: Color.WHITE,
          borderColor
        },
        style
      ]}
      onPress={toggleChecked}
      {...props}
    >
      {isChecked ? <View style={{height: size/2, width: size/2, borderRadius: size/4, backgroundColor:Color.LINK}}/> : null}
    </TouchableOpacity>
  );
};

const Styles = StyleSheet.create({
  radioButton: {
    borderWidth: DP._2,
    borderStyle: "solid"
  }
});
