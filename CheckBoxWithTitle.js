import React from "react";
import { View, Text, StyleSheet } from "react-native";
import CheckBox from "./Checkbox";
import { DP } from "./Utils/Dimen";

export const CheckBoxWithTitle = ({
  children = "",
  checkBoxstyle,
  isChecked = false,
  checkboxSize = 18,
  toggleChecked,
  activeBackgroundColor,
  inActiveBackgroundColor,
  borderColor,
  ...props
}) => {
  return (
    <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
      <Text style={Styles.text}>{children}</Text>
      <CheckBox
        size={checkboxSize}
        isChecked={isChecked}
        style={checkBoxstyle}
        toggleChecked={toggleChecked}
        activeBackgroundColor={activeBackgroundColor}
        inActiveBackgroundColor={inActiveBackgroundColor}
        borderColor={borderColor}
        {...props}
      />
    </View>
  );
};

const Styles = StyleSheet.create({
  text: {
    lineHeight: DP._16,
    fontSize: DP._14
  }
});
